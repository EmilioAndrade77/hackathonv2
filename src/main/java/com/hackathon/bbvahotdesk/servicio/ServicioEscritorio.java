package com.hackathon.bbvahotdesk.servicio;

import com.hackathon.bbvahotdesk.modelo.ModeloEscritorio;

import java.util.List;

public interface ServicioEscritorio {

    public void apartarEscritorio(ModeloEscritorio m);
    public List<ModeloEscritorio> obtenerTodos();
    public ModeloEscritorio buscarPorId(String id);
    public void guardarEstadoPorId(String id, ModeloEscritorio n);

}
