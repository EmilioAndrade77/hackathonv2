package com.hackathon.bbvahotdesk.modelo;

import java.util.List;

public class ModeloEdificio {

    List<ModeloEdificio> edificio;

    public List<ModeloEdificio> getEdificio() {
        return edificio;
    }

    public void setEdificio(List<ModeloEdificio> edificio) {
        this.edificio = edificio;
    }
}
