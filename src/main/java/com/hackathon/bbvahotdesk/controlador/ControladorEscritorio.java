package com.hackathon.bbvahotdesk.controlador;


import com.hackathon.bbvahotdesk.modelo.ModeloEscritorio;
import com.hackathon.bbvahotdesk.servicio.RepositorioEscritorio;
import com.hackathon.bbvahotdesk.servicio.ServicioEscritorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Random;


@RestController
@RequestMapping("escritorios/apartar")
public class ControladorEscritorio {

    @Autowired
    ServicioEscritorio servicioEscritorio;

    @Autowired
    RepositorioEscritorio repositorioEscritorio;


    @GetMapping
    public List<ModeloEscritorio> obtenerCitas(){
        return this.servicioEscritorio.obtenerTodos();
    }


    @PostMapping
    public void apartarEscritorio(@RequestBody ModeloEscritorio m){
        m.setId(null);
        String[] edificio = {"Polanco","Reforma","CPD"};
        String[] estatus = {"Disponible","Inhabilitado"};
        Random rand = new Random();
        Random status = new Random();
        int int_random = rand.nextInt(4);
        int int_random2 = status.nextInt(3);
        if(m.getStatusLugar().isEmpty()){
            m.setPiso(1);
            m.setStatusLugar(estatus[int_random2]);
            m.setEdificios(edificio[int_random]);
            this.servicioEscritorio.apartarEscritorio(m);

        }



    }

    @PutMapping("/{id}")
    public void cambiarEstado(@PathVariable String id,
                              @RequestBody ModeloEscritorio m){
        final ModeloEscritorio n = this.servicioEscritorio.buscarPorId(id);
        if(n == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        else if(n.getStatusLugar().equals("Ocupado")) {
            m.setNombre("");
            m.setInterno(null);
            m.setStatusLugar("Disponible");
            m.getInterno();
            this.servicioEscritorio.guardarEstadoPorId(id,m);
        }
        else if(n.getStatusLugar().equals("Disponible")){
            n.setNombre(m.getNombre());
            n.setEdificios(m.getEdificios());
            n.setPiso(m.getPiso());
            n.setStatusLugar("Ocupado");
            n.setInterno(m.getInterno());
            this.servicioEscritorio.guardarEstadoPorId(id,n);
        }



    }

    @GetMapping("/{id}")
    public ModeloEscritorio obtenerEscritorioPorId(@PathVariable String id) {
        return this.servicioEscritorio.buscarPorId(id);
    }

}
